# Simple Rest Server

This is a Simple Rest Server for file.
It servers files from a folder and sub folder.
You can also upload files to this folder.

To upload files on this folder use curl with multipart send file.

## Quick run

To run it without headache, run the command

* ./gradlew bootRun

It runs the server on the default port (8080). You can try to view http://localhost:8080/index.html


## Compile

To compile this project, use the gradle tools with:

* ./gradlew

It produces a executable jar file in this folder.


## Execute

You can execute the soft with `java -jar restfile-0.1.0.jar`

## Configuration

Add a file application.properties to configure the application.
It is a key-value files.

* server.port=80 : to setup the port of the server. Default is 8080
* file.upload-dir=./upload : the folder to upload data.
* http.static=file://${user.dir}/static/ : the folder of static element.

If you change http.static, make sure the path end with a '/' and use absolute path.
You can setup the current folder where the application runs with the ${user.dir}/.
For example, if you want to server the current folder user -Dhttp.static=file://${user.dir}/ .
Do not forget the last /
 