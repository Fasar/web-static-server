package fasar.staticserver.controller;

import fasar.staticserver.model.FileResource;
import fasar.staticserver.model.UploadFileResponse;
import fasar.staticserver.service.FileStorageException;
import fasar.staticserver.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("/api/file/**")
    public UploadFileResponse uploadFile(
            HttpServletRequest request,
            @RequestParam("file") MultipartFile file
    ) throws FileStorageException {
        String filePath = pathOf("/api/file/", request);
        String fileName = fileStorageService.storeFile(filePath, file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }


    @GetMapping("/api/file/**")
    public ResponseEntity<Resource> downloadFile(
            HttpServletRequest request
    ) {
        // Load file as Resource
        String file = pathOf("/api/file/", request);
        Resource resource = fileStorageService.loadFileAsResource(file);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    private String pathOf(String prefix, HttpServletRequest request) {
        String contextPath = request.getServletPath();
        return contextPath.replace(prefix, "");
    }

    @DeleteMapping("/api/file/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(
            @PathVariable String fileName
    ) {
        throw new RuntimeException("NOT IMPLEMENTED");
    }

    @GetMapping("/api/files")
    public List<FileResource> getFile() throws IOException {
        List<FileResource> fileResources = fileStorageService.listFile();
        return fileResources;
    }

}