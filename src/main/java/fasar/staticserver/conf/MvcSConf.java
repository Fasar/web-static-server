package fasar.staticserver.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.EncodedResourceResolver;
import org.springframework.web.servlet.resource.PathResourceResolver;


import java.io.File;
import java.io.IOException;
import java.util.List;

@Configuration
public class MvcSConf implements WebMvcConfigurer {
    private static final Logger LOG = LoggerFactory.getLogger(MvcSConf.class);

    @Autowired
    public ApplicationContext applicationContext;

    @Value("${http.static}")
    private List<String> staticBasePath;
    @Value("${user.dir}")
    private List<String> userDir;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        String[] staticPaths =new String[staticBasePath.size()];
        staticPaths = staticBasePath.toArray(staticPaths);

        for (String staticPath : staticPaths) {
            try {
                File file = applicationContext.getResource(staticPath).getFile();
                LOG.info("This server will serve : {}", file);
            } catch (IOException e) {
                LOG.error("Can't access to {}", staticPath);
                throw new RuntimeException(e);
            }
        }

        registry
                .addResourceHandler("/**")
                .addResourceLocations(staticPaths)
                .setCachePeriod(31556926)
                .resourceChain(true)
                .addResolver(new EncodedResourceResolver())
                .addResolver(new PathResourceResolver());


    }


}
