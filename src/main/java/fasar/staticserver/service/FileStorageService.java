package fasar.staticserver.service;


import fasar.staticserver.conf.FileStorageProperties;
import fasar.staticserver.model.FileResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FileStorageService {

    private Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) throws FileStorageException {
        this.fileStorageLocation = Paths
                .get(fileStorageProperties.getUploadDir())
                .toAbsolutePath()
                .normalize();
    }

    public void checkBaseFolder() throws FileStorageException {
        if(!Files.exists(this.fileStorageLocation)) {
            throw new FileStorageException("Upload folder "+ this.fileStorageLocation +" doesn't exists. Please create it manually");
        }
    }

    public String storeFile(String path, MultipartFile file) throws FileStorageException {
        checkBaseFolder();

        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            if (path.contains("..")) {
                throw new FileStorageException("Sorry! Target path contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(path);
            Path parent = targetLocation.getParent();
            if (!Files.exists(parent)) {
                try {
                    Files.createDirectories(parent);
                } catch (Exception ex) {
                    throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
                }
            }
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        checkBaseFolder();

        if (fileName.contains("..")) {
            throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
        }

        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public List<FileResource> listFile() throws IOException {
        checkBaseFolder();

        Path baseLoc = this.fileStorageLocation;

        File baseLocFile = baseLoc.toFile();
        String baseLocStr = baseLocFile.getAbsolutePath();

        List<FileResource> collect = Files.walk(baseLoc)
                .filter(e -> Files.isRegularFile(e))
                .map(path -> {

                    File fic = path.toFile();
                    String canonicalPath = null;
                    long size;
                    try {
                        size = Files.size(path);
                        canonicalPath = fic.getCanonicalPath();
                    } catch (IOException e) {
                        throw new RuntimeException("Can't get file " + path);
                    }
                    canonicalPath = canonicalPath.replace(baseLocStr, "/api/file");
                    FileResource resource = new FileResource(canonicalPath, size);
                    return resource;
                }).collect(Collectors.toList());

        return collect;
    }
}