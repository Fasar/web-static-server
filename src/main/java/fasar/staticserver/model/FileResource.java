package fasar.staticserver.model;

public class FileResource {
    private String filesName;
    private long length;

    public FileResource(String filesName, long length) {
        this.filesName = filesName;
        this.length = length;
    }

    public String getFilesName() {
        return filesName;
    }

    public void setFilesName(String filesName) {
        this.filesName = filesName;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }
}
