package fasar.staticserver;

import fasar.staticserver.conf.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class RestfileServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfileServerApplication.class, args);
    }

}
